# linden

A collection of tools for working with audio data.

Avatar image taken from [here](https://commons.wikimedia.org/wiki/File:Flowers_of_large-leaved_linden_(Tilia_platyphyllos)_IMG_3223_02.JPG), under Creative Commons 3.0.
